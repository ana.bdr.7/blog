<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
        return [
            'titulo' => $this->faker->name,
            'descripcion' => $this->faker->text,
            'id_categoria' => random_int(\DB::table('categorias')->min('id'), \DB::table('categorias')->max('id')),
            'id_user' => random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id')),
        ];
    }
}
