<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    private $categorias = array(
        array(
            'nombre' => 'actualidad'
        ),
        array(
            'nombre' => 'moda'
        ),
        array(
            'nombre' => 'mascotas'
        ),
        array(
            'nombre' => 'coches'
        ),
        array(
            'nombre' => 'cocina'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->categorias as $categoria){
            $a = new Categoria();
            $a->nombre = $categoria['nombre'];
            $a->save();

        }
        $this->command->info('tabla categorias inicializada');
    }
}
