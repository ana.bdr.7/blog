<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Seeders\CategoriaSeeder;
use Database\Seeders\TagSeeder;
use Database\Seeders\UserSeeder;
use App\Models\User;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //limpieza de tablas
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('tags_posts')->truncate();
        DB::table('posts')->truncate();
        DB::table('users')->truncate();
        DB::table('categorias')->truncate();
        DB::table('tags')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        //creacion de seeders
        DB::table('categorias')->delete();
        $this->call(CategoriaSeeder::class);
        DB::table('tags')->delete();
        $this->call(TagSeeder::class);
        DB::table('users')->delete();
        $this->call(UserSeeder::class);

        //creacion de factorias
        User::factory(5)->create();
        Post::factory(5)->create();
    }
}
