<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagSeeder extends Seeder
{
    private $tags = array(
        array(
            'nombre' => 'nature'
        ),
        array(
            'nombre' => 'moda'
        ),
        array(
            'nombre' => 'topoftheday'
        ),
        array(
            'nombre' => 'cool'
        ),
        array(
            'nombre' => 'unico'
        ),
        array(
            'nombre' => 'mascota'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->tags as $tag){
            $a = new Tag();
            $a->nombre = $tag['nombre'];
            $a->save();
        }
        $this->command->info('tabla tags inicializada');
    }
}
