@extends('layouts.master')

@section('titulo')
    blog
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-danger" role="alert">
    {{ session('mensaje') }}
</div>
@endif
<div class="container">
    

<div class="card text-center info">
  <div class="card-header info">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item info">
        <a class="nav-link info" aria-current="true" href="{{route ('admin.info')}}">Mis Datos</a>
      </li>
      <li class="nav-item info">
        <a class="nav-link active" href="#">Mis Posts</a>
      </li>
      <li class="nav-item info">
        <a class="nav-link info" href="{{route ('admin.add')}}" tabindex="-1" aria-disabled="true">Añadir Posts</a>
      </li>
    </ul>
  </div>
  @foreach($posts as $clave => $post)
  <div class="card-body">
    <h5 class="card-title">{{$post->titulo}}</h5>
    <p class="card-text">{{$post->descripcion}}</p>
    <a href="{{route ( 'admin.edit', $post->id)}}" class="btn btn-danger"><i class="fas fa-pencil-alt"></i></a>
    <a href="{{route ('admin.delete',$post->id)}}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
    
  </div>
  @endforeach
</div>


</div>
@endsection