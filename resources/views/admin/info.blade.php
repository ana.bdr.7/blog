@extends('layouts.master')

@section('titulo')
    blog administrador
@endsection

@section('contenido')
<div class="card text-center info">
  <div class="card-header info">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item info">
        <a class="nav-link active" aria-current="true" href="{{route ('admin.info')}}">Mis Datos</a>
      </li>
      <li class="nav-item info">
        <a class="nav-link info" href="{{route ('admin.posts')}}">Mis Posts</a>
      </li>
      <li class="nav-item info">
        <a class="nav-link info" href="{{route ('admin.add')}}" tabindex="-1" aria-disabled="true">Añadir Posts</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$user->name}}</h5>
    <p class="card-text">Alias : {{$user->alias}}</p>
    <p class="card-text">Alias : {{$user->email}}</p>
    <a href="#" class="btn btn-danger">Modificar Password</a>
  </div>
</div>

@endsection