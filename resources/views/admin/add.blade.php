@extends('layouts.master')

@section('titulo')
    blog
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-danger" role="alert">
    {{ session('mensaje') }}
</div>
@endif
  <div class="row">     
    <div class="offset-md-3 col-md-6">        
        <div class="card">           
            <div class="card-header text-center add">             
                 Añadir Post         
            </div>           
            <div class="card-body" style="padding:30px"> 
 
            <form method="post" action= "{{ route('admin.store') }}" >             
                    @csrf              
                    <div class="form-group">                 
                        <label for="titulo">Titulo</label>                 
                        <input type="text" name="titulo" id="titulo" class="form-control" required>              
                    </div> 

                    <div class="form-group">                                         
                        <label for="descripcion">Descripcion</label>                 
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="3"></textarea>             
                    </div> 
                    <div class="form-group">                                         
                        <label for="descripcion">Tags</label> 
                        <select class="form-select" multiple aria-label="multiple select danger" name='tags[]'>                           
                            
                            @foreach($tags as $tag)
                            <option value='{{$tag->id}}'>{{$tag->nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <fieldset class="row mb-3">
                        <legend class="col-form-label col-sm-2 pt-0">Categorias</legend>                        
                        <div class="col-sm-10">
                            @foreach($categorias as $categoria)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="id_categoria" id="id_categoria" value="{{$categoria->id}}">
                            <label class="form-check-label" for="gridRadios1">
                            {{$categoria->nombre}}
                            </label>
                        </div>
                        @endforeach
                        
                    </fieldset>



                    <div class="form-group text-center">                
                        <button type="submit" class="btn btn-danger add" style="padding:8px 100px;margin-top:25px;">             
                        Subir Post               
                    </button>              
                    </div>              
                </form>                      
            </div>       
        </div>     
    </div>  
</div>
@endsection