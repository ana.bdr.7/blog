@extends('layouts.master')

@section('titulo')
    blog
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-danger" role="alert">
    {{ session('mensaje') }}
</div>
@endif
<div class="container">
<div class="row">     
    <div class="offset-md-3 col-md-6">        
        <div class="card">           
            <div class="card-header text-center add">             
                 Editar blog           
            </div>           
            <div class="card-body" style="padding:30px"> 
 
            <form method="post" action="{{ route('admin.update',$post->id) }}">             
                    @csrf
                    @method('put')   
                               
                    <div class="form-group">                 
                        <label for="titulo">Titulo</label>                 
                        <input type="text" name="titulo" id="titulo" class="form-control" value="{{$post->titulo}}" required>              
                    </div>                    

                    <div class="form-group">                                         
                        <label for="descripcion">Descripcion</label>                 
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{$post->descripcion}}</textarea>           
                    </div> 

                    <div class="form-group">
                        <label for="categoria_id">Categoria</label>  
                        
                        <select class="form-select" aria-label="Default select example" name="id_categoria">
                            @foreach($categorias as $categoria)
                        
                                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>

                            @endforeach
                        </select>
                    </div>


                    <!--<select class="form-select" multiple aria-label="multiple select" name='id_tags[]'>
                        @foreach($tags as $tag)
                            @if(array_search($tag->id, array_column($post->tags->toArray(), 'id')) === false)
                                <option value='{{$tag->id}}'>{{$tag->nombre}}</option>
                            @else
                                <option value='{{$tag->id}}' selected>{{$tag->nombre}}</option>
                            @endif
                        @endforeach
                    </select>-->

                    <input type="hidden" name='id' value='{{$post->id}}'>
                    

                    <div class="form-group text-center">                
                    <button type="submit" class="btn btn-danger add" style="padding:8px 100px;margin-top:25px;">             
                            Modificar                
                        </button>    
                    </div>          
                </form>
            </div>       
        </div>     
    </div>  
</div>
</div>
@endsection