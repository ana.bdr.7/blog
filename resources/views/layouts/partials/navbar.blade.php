
<!-- Image and text -->
<nav class="navbar navbar-light bg-danger">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{url('/')}}">
    <i class="fal fa-comment"></i></a>
    @if(Auth::check() )
    
    <ul class="navbar-nav navbar-right">
    <li class="nav-item">
            <a href="{{ route('admin.info') }}"  class="nav-link">
              Mi Perfil
            </a>
          </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"  class="nav-link"
                  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" >
                    <span class="glyphicon glyphicon-off"></span>
                    Cerrar sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    @else
        <ul class="navbar-nav navbar-right">
            <li class="nav-item">
              <a href="{{url('login')}}" class="nav-link">Login</a>
            </li>
        </ul>
        @endif
  </div>
</nav>




