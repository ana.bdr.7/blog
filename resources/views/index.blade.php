@extends('layouts.master')

@section('titulo')
    blog
@endsection

@section('contenido')

<nav class="nav nav-pills nav-fill">
  @foreach($categorias as $categoria)
  <a class="nav-link danger" aria-current="page"  href="{{route ('index.categoria', $categoria->id)}}">{{$categoria->nombre}}</a>
  @endforeach
</nav>

<div class="card index">
@foreach($posts as $clave => $post)
  <h5 class="card-header index">{{$post->alias}}</h5>
  <div class="card-body">
    <h5 class="card-title">{{$post->titulo}}</h5>
    <p class="card-text">{{$post->descripcion}}</p>    
  </div>
  @endforeach
</div>


@endsection