<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Models\Tag_Post;
use App\Models\Categoria;
use App\Http\Controllers\TagController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\Tag_PostController;

class PostController extends Controller
{
    public function index(){
        $categorias = Categoria::all();
        $posts = DB::table('posts')
            ->join('users', 'id_user', '=', 'users.id')
            
            ->select('users.*', 'posts.*')
            ->get();

        return view('index',['posts' => $posts, 'categorias' => $categorias]);
    }

    public function getByCategoryId($id_categoria = null){

        $categorias = Categoria::all();

        $posts = DB::table('posts')
            ->join('users', 'id_user', '=', 'users.id')            
            ->select('users.*', 'posts.*')
            ->where('id_categoria', $id_categoria)->paginate(6);
        
        return view('index',['posts' => $posts, 'categorias' => $categorias]);
    }

    public function show(Request $request){

        $user = $request->user()->id;

        $posts = DB::table('posts')
            ->where('id_user',$user)->get();
            
        
        return view('admin.posts',['posts' => $posts]);
    }

    public function create(){

        $tags = DB::table('tags')->get();
        $categorias = DB::table('categorias')->get();

        return view('admin.add', ['tags' => $tags, 'categorias' => $categorias]);
    }

    public function store(Request $request){
        
        $save['titulo'] = $request->titulo;
        $save['descripcion'] = $request->descripcion;
        $save['id_user'] = $request->user()->id;
        $save['id_categoria'] = $request->id_categoria;


        try{
            
            $post = Post::create($save);
            $post->tags()->sync($request->tags);

            
            return redirect()->route('admin.posts')->with('mensaje','Post subido correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            return redirect()->route('admin.posts')->with('mensaje','Fallo al subir el animal');
        }
           
    }

    public function delete($id){

        $post = Post::find($id);
        
        $post->tags()->detach($post->tags);

        $post->delete();
     
        return redirect()->route('admin.posts')->with('Post borrado correctamente');

    }

    public function edit($id){

        $categorias = Categoria::all();
        $tags = Tag::all();

        //$post = DB::table('posts')->where('id', $id)->first();

        $post = Post::find($id);

        return view('admin.edit',['post' => $post, 'categorias' => $categorias, 'tags' => $tags]);

    }

    public function update(Request $request,Post $post){

        //buscamos el id del post
        $post = Post::find($request->id);

        //colocamos los atributos que vamos a modificar
        $post->id_user = $request->user()->id;
        $post->titulo = $request->titulo;        
        $post->descripcion = $request->descripcion;       
        $post->id_categoria = $request->id_categoria;        
        
        
        //comienza la transaccion
         try{
            
            //salva el post
            $post->save();

            //modifica los tags en la tabla intermedia        
            $post->tags()->sync($request->id_tags);

            //redirigie a la ruta            
            return redirect()->route('admin.posts')->with('Post modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   
                     
            return redirect()->route('admin.posts')->with('Fallo al modificar el post');
        }
        
        
        
        
}
}
