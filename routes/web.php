<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index'])->name('home');

Route::get("index/{categoria}", [PostController::class , 'getByCategoryId'])
    ->name('index.categoria');

Route::get('admin/info', [UserController::class, 'user'])
    ->middleware('auth')
    ->name('admin.info');

Route::get('admin/posts', [PostController::class, 'show'])
    ->middleware('auth')
    ->name('admin.posts');

Route::get('admin/add', [PostController::class, 'create'])
    ->middleware('auth')
    ->name('admin.add');

Route::post('admin/add',[PostController::class,'store'])
    ->middleware('auth')
    ->name('admin.store');

Route::get('admin/delete/{blog}', [PostController::class, 'delete'])
    ->middleware('auth')
    ->name('admin.delete');

Route::get('admin/edit/{blog}', [PostController::class, 'edit'])
    ->middleware('auth')
    ->name('admin.edit');

Route::put("admin/posts",[PostController::class, 'update'])
    ->name('admin.update')
    ->middleware('auth');


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
